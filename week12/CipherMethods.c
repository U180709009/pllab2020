#include "CipherMethods.h"

char * encryptCaesar(const char *text){

char* enc_text=malloc(strlen(text));
int y=0;

for(int i = 0; i < 500; i = i+1){
  if(text[i]=='\0'){
    enc_text[i]=text[i];
    break;
  }
  //to check "is it special chars"
  if(strchr(CIPHER_SPECIAL_CHARS, text[i])){
    enc_text[y] = text[i];
    y++;
  }
  //to check "is it upper case"
  else if(text[i]>64 && text[i]<91){

    if(text[i] == 88){
      enc_text[y] = 65;
      y++;
    }
    else if(text[i] == 89){
      enc_text[y] = 66;
      y++;
    }
    else if(text[i] == 90){
      enc_text[y] = 67;
      y++;
    }
    else{
      enc_text[y] = text[i]+CAESAR_SHIFT;
      y++;
    }
  }

   //to check "is it lower case" 
   else if (text[i]>96 && text[i]<123){

     if(text[i] == 120){
      enc_text[y] = 97;
      y++;
    }
    else if(text[i] == 121){
      enc_text[y] = 98;
      y++;
    }

    else if(text[i] == 122){
      enc_text[y] = 99;
      y++;
    }
    else{
      enc_text[y] = text[i]+CAESAR_SHIFT;
      y++;
    }
   }

   //to check the number
   else{
     if(text[i] == 55){
      enc_text[y] = 48;
      y++;
    }
    else if(text[i] == 56){
      enc_text[y] = 49;
      y++;
    }

    else if(text[i] == 57){
      enc_text[y] = 50;
      y++;
    }
    else{
      enc_text[y] = text[i]+CAESAR_SHIFT;
      y++;
    }
   }

  }

return enc_text;
}


char* decryptCaesar(const char *cipher_text){
  
  char* decr_text=malloc(strlen(cipher_text));
  int y=0;

  for(int i = 0; i < 500; i = i+1){
   if(cipher_text[i]=='\0'){
    decr_text[i]=cipher_text[i];
    break;
  }
  //to check "is it special chars"
  if(strchr(CIPHER_SPECIAL_CHARS, cipher_text[i])){
    decr_text[y] = cipher_text[i];
    y++;
  }
  //to check "is it upper case"
  else if(cipher_text[i]>64 && cipher_text[i]<91){

    if(cipher_text[i] == 65){
      decr_text[y] = 88;
      y++;
    }
    else if(cipher_text[i] == 66){
      decr_text[y] = 89;
      y++;
    }
    else if(cipher_text[i] == 67){
      decr_text[y] = 90;
      y++;
    }
    else{
      decr_text[y] = cipher_text[i]-CAESAR_SHIFT;
      y++;
    }
  }

   //to check "is it lower case" 
   else if (cipher_text[i]>96 && cipher_text[i]<123){

     if(cipher_text[i] == 97){
      decr_text[y] = 120;
      y++;
    }
    else if(cipher_text[i] == 98){
      decr_text[y] = 121;
      y++;
    }

    else if(cipher_text[i] == 99){
      decr_text[y] = 122;
      y++;
    }
    else{
      decr_text[y] = cipher_text[i]-CAESAR_SHIFT;
      y++;
    }
   }

   //to check the number
   else{
     if(cipher_text[i] == 48){
      decr_text[y] = 55;
      y++;
    }
    else if(cipher_text[i] == 49){
      decr_text[y] = 56;
      y++;
    }

    else if(cipher_text[i] == 50){
      decr_text[y] = 57;
      y++;
    }
    else{
      decr_text[y] = cipher_text[i]-CAESAR_SHIFT;
      y++;
    }
   }

  }

  return decr_text;
}
