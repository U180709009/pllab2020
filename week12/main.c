#include <stdio.h>
#include "CipherMethods.h"
#define TRACE_ORG_DEC


int main(void) {
  
  char text[500];
  char* decrypted_text;
  char *cipher_text;
  char answer[1];
  char passcode[9] = "ceng2002";
  char passcode_ent[9];

  printf("** You are about to enter a very secret cryptography service called CENG 2002 C-Secret Coded System **\n\n Enter your text: \n\n");

  fgets(text, 500, stdin);
  system ("clear"); 

  printf("text is %s\n",text);
  printf("In order to see the encrypted message, enter your passcode:\n"); 
  scanf("%s", passcode_ent);
  
  int count=1;
  while(!(strstr(passcode_ent, passcode))){

    printf("Wrong passcode. Enter again:\n"); 
    scanf("%s", passcode_ent);
    count++;
    if(count == 3){
      printf("Number of allowed attempts has been reached without successful entry!\nYour IP has been blacklisted by us. Good luck!!");
      return 0;
    }
  }
  
  printf("Cipher: ");
  cipher_text = encryptCaesar(text);
  printf("%s\n",cipher_text);
    

  printf("\n\nWould you like to convert the cipher to original text (Y/N)?\n");
  scanf("\n%c", answer);

  if(strncmp(answer, "Y", 1)==0){
      decrypted_text = decryptCaesar(cipher_text);
      printf("%s\n",decrypted_text);
  }
  else{
  return 0;
  }

#ifdef TRACE_ORG_DEC
  printf(strcmp(text, decrypted_text) == 0 ? "Original text and the decrypted text match." : "There is something wrong. Original text and the decrypted text do not match.");
  printf("\n\n");
#endif
  return 0;
}