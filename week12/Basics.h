#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#define NUMBER_OF_LETTERS 26
#define NUMBER_OF_DIGITS 10
#define CIPHER_SPECIAL_CHARS " .!?,;:'()[]{}`\n"
#define CAESAR_SHIFT 3